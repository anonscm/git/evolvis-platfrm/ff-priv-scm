<?php
/*-
 * Copyright © 2013
 *	mirabilos <t.glaser@tarent.de>
 * All rights reserved.
 *
 * This file is part of FusionForge. FusionForge is free software;
 * you can redistribute it and/or modify it under the terms of the
 * GNU General Public License as published by the Free Software
 * Foundation; either version 2 of the Licence, or (at your option)
 * any later version.
 *
 * FusionForge is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with FusionForge; if not, write to the Free Software Foundation, Inc.,
 * 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 *-
 * Private SCM Browsing module for FusionForge: plug-in component
 */

class PrivSCMPlugin extends Plugin {

	function __construct() {
		parent::__construct();
		$this->name = 'privscm';
		$this->text = _('Private SCM Browsing Plugin');
		$this->hooks[] = 'blocks';
	}

	function CallHook($hookname, &$params) {
		switch ($hookname) {
		case 'blocks':
			switch ($params) {
			case "scm index":
				$this->show_index();
				break;
			}
			break;
		}
	}

	function show_index() {
		global $HTML;
		global $group;

		$type = false;
		foreach (array(
			'git' => 'scmgit',
			'svn' => 'scmsvn',
			/* must be last */
			'cvs' => 'scmcvs'
		    ) as $itype => $iplugin) {
			if ($group->usesPlugin($iplugin)) {
				$type = $itype;
				break;
			}
		}
		if (!$type) {
			return;
		}
		$pages = array(
			'cvs' => 'priv_cvs.cgi/?root=%s',
			'git' => 'priv_git.cgi?pf=%s',
			'svn' => 'priv_svn.cgi/?root=%s',
		    );
		$page = sprintf('/plugins/privscm/cgi-bin/' .
		    $pages[$type], $group->getUnixName());

		print $HTML->boxTop(_('SCM Private Browsing')) . '<p>' .
		    sprintf(_('As a logged-in user, you can also privately <a href="%s">browse this SCM repository</a> if you have commit access.'),
		    util_html_encode(util_make_url($page))) . '</p>' .
		    $HTML->boxBottom();
	}

}
